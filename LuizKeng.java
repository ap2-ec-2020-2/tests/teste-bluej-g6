public class LuizKeng
{
    String nome = "Luiz";
    String sobrenome = "Keng";
    String semestre = "2020/2";
    
    String nomeCompleto () {
        return this.nome + " " + this.sobrenome;
    }
    
    String getSemestre () {
        return this.semestre;
    }
}

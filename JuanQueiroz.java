public class JuanQueiroz  {
    private String nome = "Juan";
    private String sobrenome = "Queiroz";
    private String semestre = "2020/2";
    
    public String nomeCompleto() {
        return this.nome + " " + this.sobrenome;
    }
    
    public String getSemestre() {
        return this.semestre;
    }
    
}
